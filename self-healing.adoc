= Self-Healing Infrastructure
Camry Fedei @cfedei
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


Modern application infrastructure has, in many ways, gotten more complex as it has become more powerful and easy to consume.
Keeping that infrastructure safe and compliant is a challenge for many organizations. One of the most powerful approaches to 
infrastructure management today is the combination of using historical data-driven insights, and automation tools for applying 
remediation across a scaling estate of hosts in a targeted and prioritized manner.

Use case: Managing security, policy and patches for a large number of servers in data centers or public/private clouds.

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/self-healing.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/blob/main/diagrams/self-healing.drawio?inline=false[[Download Diagram]]
--

--
image:logical-diagrams/self-healing-ld.png[350, 300]
image:schematic-diagrams/self-healing-sd-net.png[350, 300]
image:schematic-diagrams/self-healing-sd-data.png[350, 300]
image:detail-diagrams/self-healing-detail-smartmanagement.png[250, 200]
image:detail-diagrams/self-healing-detail-automationorchestration.png[250, 200]
--
